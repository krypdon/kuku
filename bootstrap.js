/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is Full Screen Mobile Add-on.
 *
 * The Initial Developer of the Original Code is
 * Mozilla Corporation.
 * Portions created by the Initial Developer are Copyright (C) 2011
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 * * Matt Brubeck <mbrubeck@mozilla.com>
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the LGPL or the GPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK ***** */

"use strict";

var Cc = Components.classes;
var Ci = Components.interfaces;
var Cu = Components.utils;

Cu.import("resource://gre/modules/Services.jsm");

const prefName = "general.useragent.override";
let gWin;

function isNativeUI() {
  let appInfo = Cc["@mozilla.org/xre/app-info;1"].getService(Ci.nsIXULAppInfo);
  return (appInfo.ID == "{aa3c5121-dab2-40e2-81ca-7ea25febc110}");
}

let Phony = {
  set useragent(value) {
    if (value)
      Services.prefs.setCharPref(prefName, value);
    else if (Services.prefs.prefHasUserValue(prefName))
      Services.prefs.clearUserPref(prefName);
  },

  get useragent() {
    return Services.prefs.prefHasUserValue(prefName)
         ? Services.prefs.getCharPref(prefName) : "";
  }
};

let gMenuId = null;
let gStringBundle = null;

// This should be kept in sync with the values in locale/*/phony.properties
let uas = [
  ["Default", ""],
  ["Desktop Firefox", "Mozilla/5.0 (X11; Linux x86_64; rv:19.0) Gecko/20100101 Firefox/19.0"],
  ["Android (Phone)", " Mozilla/5.0 (Linux; U; Android 4.0.2; en-us; Galaxy Nexus Build/ICL53F) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30"],
  ["Android (Tablet)", "Mozilla/5.0 (Linux; U; Android 4.0.4; en-us; Xoom Build/IMM76) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Safari/534.30"],
  ["Opera Mobile", "Opera/9.80 (S60; SymbOS; Opera Mobi/SYB-1107071606; U; en) Presto/2.8.149 Version/11.10"],
  ["iPhone", "Mozilla/5.0 (iPhone; CPU iPhone OS 5_0_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A405 Safari/7534.48.3"],
  ["iPhone 5S", "Mozilla/5.0 (iPhone; CPU iPhone OS 7_0_1 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A470a Safari/9537.53"],
  ["iPad", "Mozilla/5.0 (iPad; CPU OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3"],
  ["iPad Mini", "Mozilla/5.0 (iPad; CPU OS 6_0_2 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A550 Safari/8536.25"],
  ["iPod Touch 4", "Mozilla/5.0 (iPod; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A403 Safari/8536.25"],
  ["BlackBerry Z10", "Mozilla/5.0 (BB10; Touch) AppleWebKit/537.10+ (KHTML, like Gecko) Version/10.0.9.2372 Mobile Safari/537.10+"],
  ["BlackBerry Tablet", "Mozilla/5.0 (PlayBook; U; RIM Tablet OS 2.0.0; en-US) AppleWebKit/535.8+ (KHTML, like Gecko) Version/7.2.0.0 Safari/535.8+"],
  ["IE Lumia 920", "Mozilla/5.0 (compatible; MSIE 10.0; Windows Phone 8.0; Trident/6.0; IEMobile/10.0; ARM; Touch; NOKIA; Lumia 920)"],
  ["IE Lumia 820", "Mozilla/5.0 (compatible; MSIE 10.0; Windows Phone 8.0; Trident/6.0; IEMobile/10.0; ARM; Touch; NOKIA; Lumia 820)"],
  ["Microsoft IE", "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)"],
  ["Nokia 311", "Mozilla/5.0 (Series40; Nokia311/03.81; Profile/MIDP-2.1 Configuration/CLDC-1.1) Gecko/20100401 S40OviBrowser/2.2.0.0.31"],
  ["Nokia E75", "Mozilla/5.0 (SymbianOS/9.3; U; Series60/3.2 NokiaE75-1/110.48.125 Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/413 (KHTML, like Gecko) Safari/413"],
  ["Nokia N9", "Mozilla/5.0 (MeeGo; NokiaN9) AppleWebKit/534.13 (KHTML, like Gecko) NokiaBrowser/8.5.0 Mobile Safari/534.13"],
  ["Chrome", "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.97 Safari/537.22"],
  ["Chrome Win7 x64", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.11 (KHTML like Gecko) Chrome/17.0.963.46 Safari/535.11"],
];

function init(aEvent) {
  if (this.selectedPanel.id != "prefs-container")
    return;

  let document = this.ownerDocument;
  let menu = document.getElementById("phony-useragent-menu");

  if (!menu) {
    // Create the menu.
    let setting = document.createElement("setting");
    setting.setAttribute("id", "phony-useragent-setting");
    setting.setAttribute("type", "control");
    setting.setAttribute("title", "User Agent");
    document.getElementById("prefs-list").insertBefore(setting, document.getElementById("prefs-sync"));

    menu = document.createElement("menulist");
    menu.setAttribute("id", "phony-useragent-menu");
    setting.appendChild(menu);
  }

  // At this point (on Android devices only?), appending items to the menu does
  // not work.  It seems the XBL binding is not always available immediately,
  // so we need a timeout before using it.
  gWin.setTimeout(function() {
    if (!menu.firstChild || !menu.firstChild.hasChildNodes) {
      for (let i=0; i<uas.length; i++) {
        let [label, value] = uas[i];
        menu.appendItem(label, value); // XXX not available until XBL kicks in
      }
      menu.addEventListener("select", function() Phony.useragent = menu.selectedItem.value, false);
    }

    let item = menu.getElementsByAttribute("value", Phony.useragent)[0];
    if (!item)
      item = menu.appendItem("Custom", Phony.useragent);
    menu.selectedItem = item;
  }, 0);
}

function initUAList() {
  if (!gStringBundle)
    gStringBundle = Services.strings.createBundle("chrome://phony/locale/phony.properties");

  let uaItemLabel, uaItemValue = null;

  for (let i=0; i<uas.length; i++) {
    try {
      uaItemLabel = gStringBundle.GetStringFromName("phony.uas." + i);
      uaItemValue = gStringBundle.GetStringFromName("phony.uas." + i + ".value");
      if (uaItemLabel) {
        uas[i] = [uaItemLabel, uaItemValue];
      }
    } catch (e) { Cu.reportError(e); }
  }
}

function selectUA() {
  let labels = [];
  let values = [];
  let found = false;

  for (let i = 0; i < uas.length; i++) {
    let [label, value] = uas[i];
    if (value == Phony.useragent) {
      found = true;
      labels.unshift(label);
      values.unshift(value);
    } else {
      labels.push(label);
      values.push(value);
    }
  }

  if (!found) {
    labels.push("Custom");
    values.push(Phony.useragent);
  }

  let res = { value: 0 };
  if (!gStringBundle)
    gStringBundle = Services.strings.createBundle("chrome://phony/locale/phony.properties");
  let selectTitleString = gStringBundle.GetStringFromName("phony.name");
  let selectMsgString = gStringBundle.GetStringFromName("phony.selectMsg");
  if (Services.prompt.select(null, selectTitleString, selectMsgString, labels.length, labels, res))
    Phony.useragent = values[res.value];
}

function load(win) {
  initUAList();

  gWin = win;
  if (isNativeUI()) {
    gMenuId = win.NativeWindow.menu.add("Phony", null, selectUA);
  } else {
    let panels = win.document.getElementById("panel-items");
    panels.addEventListener("select", init, false);
  }
}

function unload(win) {
  if (isNativeUI()) {
    win.NativeWindow.menu.remove(gMenuId);
  } else {
    let setting = win.document.getElementById("phony-useragent-setting");
    if (setting)
      setting.parentNode.removeChild(setting);
    win.document.getElementById("panel-items").removeEventListener("select", init, false);
  }
  Phony.useragent = null;
}

var listener = {
  onOpenWindow: function(aWindow) {
    // Wait for the window to finish loading
    let win = aWindow.QueryInterface(Ci.nsIInterfaceRequestor)
                     .getInterface(Ci.nsIDOMWindowInternal || Ci.nsIDOMWindow);
    win.addEventListener("UIReady", function onReady(aEvent) {
      win.removeEventListener("UIReady", onReady, false);
      load(win);
    }, false);
  },

  // Unused:
  onCloseWindow: function(aWindow) { },
  onWindowTitleChange: function(aWindow, aTitle) { }
};

/* Bootstrap Interface */

function startup(aData, aReason) {
  // Load in existing windows.
  let enumerator = Services.wm.getEnumerator("navigator:browser");
  while(enumerator.hasMoreElements()) {
    let win = enumerator.getNext();
    load(win);
  }

  // Load in future windows.
  Services.wm.addListener(listener);
}

function shutdown(aData, aReason) {
  if (aReason == APP_SHUTDOWN)
    return;

  Services.wm.removeListener(listener);

  let enumerator = Services.wm.getEnumerator("navigator:browser");
  while(enumerator.hasMoreElements()) {
    let win = enumerator.getNext();
    unload(win);
  }
}

function install(aData, aReason) {}
function uninstall(aData, aReason) {}
